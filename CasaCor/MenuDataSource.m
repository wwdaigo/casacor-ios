//
//  MenuDataSource.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 15/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "MenuDataSource.h"

@implementation MenuDataSource

-(id)init
{
    if (self = [super init])
    {
        MenuHelper * helper = [MenuHelper new];
        [super setList:[NSMutableArray arrayWithArray:[helper list]]];
    }
    
    return self;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell" forIndexPath:indexPath];
    
    MenuData * data = [[super list] objectAtIndex:indexPath.row];
    
    UIImageView * image = (UIImageView *)[cell viewWithTag:1];
    UILabel * lblName = (UILabel *)[cell viewWithTag:2];
    
    [image setImage:data.image];
    [lblName setText:data.title];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"menuOption" object:[[[super list] objectAtIndex:indexPath.row] action]];
}


@end
