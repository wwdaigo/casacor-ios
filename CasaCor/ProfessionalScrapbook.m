//
//  ProfessionalScrapbook.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 24/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "ProfessionalScrapbook.h"


@implementation ProfessionalScrapbook

@dynamic isSent;
@dynamic oid;
@dynamic professionalId;
@dynamic text;

@end
