//
//  RestConnection.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 21/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "RestConnection.h"
#import "EditionsDAO.h"
#import "AmbiancesDAO.h"
#import "AmbiancePhotosDAO.h"
#import "AmbianceProfessionalDAO.h"
#import "ProfessionalsDAO.h"
#import "AmbianceProductsDAO.h"
#import "ProductsDAO.h"
#import "SupplierDAO.h"


@implementation RestConnection

-(id)init
{
    self = [super init];
    
    NSURLSessionConfiguration * sessionConfig =  [NSURLSessionConfiguration defaultSessionConfiguration];
    urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
    
    return self;
}

#pragma mark - Public methods
-(void)getEvents
{
    NSString * strURL = [NSString stringWithFormat:@"%@events",RESTURL];
    NSURL * url = [NSURL URLWithString:strURL];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    [request addValue:TOKEN forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionDataTask * dataTask = [urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSError * err;
        NSArray * arrList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
        
        EditionsDAO * dao = [EditionsDAO new];
        
        [dao clear];

        for (NSDictionary * dict in arrList)
        {
            NSNumber * oid = [dict objectForKey:@"id"];
            Editions * data = [dao objectByOID:oid];
            
            [data setName:[self sanitize:[dict objectForKey:@"name"]]];
            [data setLocation:[self sanitize:[dict objectForKey:@"venue"]]];
            [data setCity:[self sanitize:[dict objectForKey:@"address_city"]]];
            [data setState:[self sanitize:[dict objectForKey:@"address_state"]]];
            
            
            if ([dict objectForKey:@"lat"] != [NSNull null])
            {
                [data setLatitude:[self sanitize:[dict objectForKey:@"lat"]]];
                [data setLongitude:[self sanitize:[dict objectForKey:@"long"]]];
            }
            
            
            [data setTicketUrl:[self sanitize:[dict objectForKey:@"ticket_url"]]];
            [data setVotingUrl:[self sanitize:[dict objectForKey:@"voting_url"]]];
            
            [data setMap:[self sanitize:[dict objectForKey:@"map"]]];
            
            [dao save:data];
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate restConnection:self succeededEntity:@"Events"];
        });
                                           
    }];
    
    [dataTask resume];
}

-(void)getAmbiancesWithEventID:(NSNumber *)eventOID
{
    NSString * strURL = [NSString stringWithFormat:@"%@events/%@/ambiances",RESTURL,eventOID];
    NSURL * url = [NSURL URLWithString:strURL];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    [request addValue:TOKEN forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionDataTask * dataTask = [urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSError * err;
        NSArray * arrList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
        
        AmbiancesDAO * dao = [AmbiancesDAO new];

        [dao clearWithObjects:[dao search:[NSString stringWithFormat:@"editionId == %@", eventOID]]];
        for (NSDictionary * dict in arrList)
        {
            
            NSNumber * oid = [dict objectForKey:@"id"];
            
            Ambiances * data = [dao objectByOID:oid];
            
            [data setName:[self sanitize:[dict objectForKey:@"name"]]];
            [data setEditionId:eventOID];
            if ([dict objectForKey:@"position"] == [NSNull null])
                [data setPosition:@0];
            else
                [data setPosition:[self sanitize:[dict objectForKey:@"position"]]];
            
            if ([dict objectForKey:@"quiz"] != [NSNull null])
            {
                [data setQuiz:[self sanitize:[dict objectForKey:@"quiz"]]];
                [data setQuizHTML:[self sanitize:[dict objectForKey:@"quiz_html"]]];
            }
            
            NSArray * arrPhotos = [dict objectForKey:@"photos"];
            AmbiancePhotosDAO * photoDAO = [AmbiancePhotosDAO new];
            
            [photoDAO clearWithObjects:[photoDAO search:[NSString stringWithFormat:@"ambianceId == %@", oid]]];
            
            for (NSDictionary * dictPhoto in arrPhotos)
            {
                AmbiancePhotos * photo = [photoDAO objectByOID:[dictPhoto objectForKey:@"id"]];
                [photo setAmbianceId:oid];
                [photo setPhoto:[dictPhoto objectForKey:@"photo"]];
                [photoDAO save:photo];
            }
            
            NSArray * arrPros = [dict objectForKey:@"professionals"];
            AmbianceProfessionalDAO * ambProDAO = [AmbianceProfessionalDAO new];
            ProfessionalsDAO * profDAO = [ProfessionalsDAO new];
            

#warning Rever
          //  [ambProDAO clear];
            
            for (NSDictionary * dictPro in arrPros)
            {
                
                
                
                Professionals * proObj = [profDAO objectByOID:[dictPro objectForKey:@"id"]];
                [proObj setName:[dictPro objectForKey:@"name"]];
                [proObj setPhoto:[dictPro objectForKey:@"picture"]];
                [profDAO save:proObj];
                
                NSArray * list = [ambProDAO search:[NSString stringWithFormat:@"professionalId = %@",[dictPro objectForKey:@"id"]]];
                
                AmbianceProfessional * ambPro = [ambProDAO objectByAmbienceId:oid professionalId:[dictPro objectForKey:@"id"]];
                [ambProDAO save:ambPro];
            }
                                               
            [dao save:data];
        }
                                           
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate restConnection:self succeededEntity:@"Ambiances"];
        });
                                           
    }];
    
    [dataTask resume];
}


-(void)getAmbiancesProdutctsWithID:(NSNumber *)ambianceOID
{
    NSString * strURL = [NSString stringWithFormat:@"%@ambiance_photos/%@/products",RESTURL,ambianceOID];
    NSLog(@"strURL %@",strURL);
    NSURL * url = [NSURL URLWithString:strURL];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    [request addValue:TOKEN forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionDataTask * dataTask = [urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           NSError * err;
                                           NSArray * listArr = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
                                           NSLog(@"listArr %@",listArr);

                                           AmbianceProductsDAO * dao = [AmbianceProductsDAO new];
                                           [dao clear];
                                           //[dao clearWithObjects:[dao search:[NSString stringWithFormat:@"ambiancePhotoId == %@", ambianceOID]]];
                                           for (NSDictionary * dict in listArr)
                                           {
                                               NSNumber * oid = [dict objectForKey:@"id"];
                                               
                                               AmbianceProducts * photo = [dao objectByOID:oid];
                                               [photo setAmbiancePhotoId:ambianceOID];
                                               [photo setPosX:[dict objectForKey:@"x"]];
                                               [photo setPosY:[dict objectForKey:@"y"]];
                                               [photo setProductId:[[dict objectForKey:@"product"] objectForKey:@"id"]];
                                               
                                               [dao save:photo];
                                           }

                                           dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate restConnection:self succeededEntity:@"AmbiancesProducts"];
        });
                                        
    }];
    
    [dataTask resume];
}


-(void)getProfessionalWithId:(NSNumber *)oid
{
    NSString * strURL = [NSString stringWithFormat:@"%@professionals/%@",RESTURL,oid];
    NSURL * url = [NSURL URLWithString:strURL];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    [request addValue:TOKEN forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionDataTask * dataTask = [urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           NSError * err;
                                           NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
                                           NSNumber * oid = [dict objectForKey:@"id"];
                                           
                                           ProfessionalsDAO * dao = [ProfessionalsDAO new];
                                           
                                           Professionals * professional = [dao objectByOID:oid];
                                           
                                           [professional setEmail:[self sanitize:[dict objectForKey:@"email"]]];
                                           [professional setWebsite:[self sanitize:[dict objectForKey:@"site"]]];
                                           
                                           [dao save:professional];
                                                                                     
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [self.delegate restConnection:self succeededEntity:@"Professional"];
                                           });
                                           
                                       }];
    
    [dataTask resume];
}


-(void)getProductWithId:(NSNumber *)oid
{
    NSString * strURL = [NSString stringWithFormat:@"%@products/%@",RESTURL,oid];
    NSURL * url = [NSURL URLWithString:strURL];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    [request setHTTPMethod:@"GET"];
    
    [request addValue:TOKEN forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionDataTask * dataTask = [urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        NSError * err;
        NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
        NSNumber * oid = [dict objectForKey:@"id"];
        NSDictionary * supplierDict = [dict objectForKey:@"supplier"];
                                           
        ProductsDAO * dao = [ProductsDAO new];
        Products * product = [dao objectByOID:oid];
        [product setName:[self sanitize:[dict objectForKey:@"name"]]];
        [product setSegment:[self sanitize:[dict objectForKey:@"segment"]]];
        [product setPhoto:[self sanitize:[dict objectForKey:@"photo"]]];

        [product setSupplierId:[supplierDict objectForKey:@"id"]];
                                           
        SupplierDAO * supplierDAO = [SupplierDAO new];
        Supplier * supp = [supplierDAO objectByOID:[supplierDict objectForKey:@"id"]];
        [supp setName:[supplierDict objectForKey:@"name"]];
                                           [supplierDAO save:supp];

                                           [dao save:product];
                                           
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [self.delegate restConnection:self succeededEntity:@"Product"];
                                           });
                                           
    }];
    
    [dataTask resume];
}


-(void)postMessageWithName:(NSString *)name email:(NSString *)email ambianceId:(NSNumber *)ambianceId message:(NSString *)message
{
    NSError * error;
    
    NSString * strURL = [NSString stringWithFormat:@"%@ambiance_messages",RESTURL];
    NSURL * url = [NSURL URLWithString:strURL];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];

    NSDictionary *mapData = [[NSDictionary alloc] initWithObjectsAndKeys: ambianceId, @"ambiance_id", name, @"name", email, @"email", message, @"message", nil];

    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [urlSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
/*            NSError * err;
            NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
*/
            [self.delegate restConnection:self succeededEntity:@"PostMessage"];
        });
    }];
    
    [postDataTask resume];
}

//name, email, ambiance_id, message

-(id)sanitize:(id)value
{
    if (value != [NSNull null])
        return value;
    else
        return @"";
}



@end
