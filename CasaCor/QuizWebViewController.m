//
//  QuizWebViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 14/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "QuizWebViewController.h"
#import "EditionsDAO.h"
#import "PropertiesHelper.h"

@implementation QuizWebViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    EditionsDAO * dao = [EditionsDAO new];
    Editions * edition = [dao objectByOID:[PropertiesHelper chosenEdition]];
    
    [self openURL:[NSURL URLWithString:edition.votingUrl]];
}


@end
