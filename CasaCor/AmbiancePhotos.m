//
//  AmbiancePhotos.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 24/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "AmbiancePhotos.h"


@implementation AmbiancePhotos

@dynamic ambianceId;
@dynamic oid;
@dynamic photo;

@end
