//
//  ImageHelper.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 26/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "ImageHelper.h"

@implementation ImageHelper

+(BOOL)imageWithURLString:(NSString *)urlstring prefix:(NSString *)prefix oid:(NSNumber *)oid andCompletionHandler:(void(^)(UIImage *image))completionHandler
{
    NSURL * url = [NSURL URLWithString:urlstring];
    
    if (![urlstring isEqualToString:@""])
    {
        NSString * filename = [NSString stringWithFormat:@"/%@-%@.jpg",prefix,oid];
        NSString *imagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:filename];
        
        UIImage * image = [UIImage imageNamed:imagePath];
        
        if (image == nil)
        {
            
            NSURLSessionConfiguration * sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
            
            sessionConfig.timeoutIntervalForRequest = 30.0;
            sessionConfig.timeoutIntervalForResource = 60.0;
            sessionConfig.HTTPMaximumConnectionsPerHost = 1;
            
            NSURLSession * session = [NSURLSession sessionWithConfiguration:sessionConfig];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            
            NSURLSessionDownloadTask * downloadTask = [session downloadTaskWithRequest:request completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                
                NSData * imageData = [NSData dataWithContentsOfURL:location];
                
                [imageData writeToFile:imagePath atomically:YES];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage * image = [UIImage imageWithData:imageData];

                    completionHandler(image);

                });
            }];
            
            [downloadTask resume];

            return false;
        }
        else
        {
            completionHandler (image);
            return true;
        }
    }
    else
    {
        completionHandler(nil);
    }
    
    return false;
}

@end
