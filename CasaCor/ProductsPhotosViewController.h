//
//  ProductsPhotosViewController.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 17/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "PhotosViewController.h"


@interface ProductsPhotosViewController : PhotosViewController

@property (nonatomic, strong) UIImage * img;

@property (nonatomic, strong) IBOutlet UIImageView * imageView;

@end
