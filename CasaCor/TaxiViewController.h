//
//  TaxiViewController.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 27/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "WebViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface TaxiViewController : WebViewController <CLLocationManagerDelegate>
{
    BOOL requested;
}

@property (nonatomic, strong) CLLocationManager * locationManager;

@end
