//
//  Ambiances.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 27/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "Ambiances.h"


@implementation Ambiances

@dynamic editionId;
@dynamic name;
@dynamic oid;
@dynamic position;
@dynamic quizHTML;
@dynamic quiz;

@end
