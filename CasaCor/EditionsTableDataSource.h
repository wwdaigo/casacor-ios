//
//  EditionsTableDataSource.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 16/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "AbstractTableDataSource.h"
#import "EditionsDAO.h"

@interface EditionsTableDataSource : AbstractTableDataSource

@end
