//
//  MainTitleView.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 16/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "MainTitleView.h"

@implementation MainTitleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(IBAction)openEditionPicker:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"openEditionPicker" object:nil];
}

-(void)setCity:(NSString *)city
{
    [self.lblCity setText:city];
}

@end
