//
//  AmbianceQuizViewController.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 27/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "WebViewController.h"
#import "AmbiancesDAO.h"

@interface AmbianceQuizViewController : WebViewController

@property (nonatomic, strong) Ambiances * ambiance;

@end
