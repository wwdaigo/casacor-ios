//
//  AmbienceProfessionalDAO.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 14/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "DAOAbstract.h"

#import "AmbianceProfessional.h"

@interface AmbianceProfessionalDAO : DAOAbstract

-(AmbianceProfessional *)objectByOID:(NSNumber *)oid;
-(AmbianceProfessional *)objectByAmbienceId:(NSNumber *)ambienceId professionalId:(NSNumber *)professionalId;

@end
