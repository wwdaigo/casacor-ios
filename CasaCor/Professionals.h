//
//  Professionals.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 24/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Professionals : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * oid;
@property (nonatomic, retain) NSString * photo;
@property (nonatomic, retain) NSString * website;

@end
