//
//  CloudinaryUtils.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 04/11/15.
//  Copyright © 2015 Muuving. All rights reserved.
//

#import "CloudinaryUtils.h"

@implementation CloudinaryUtils

+(NSString *)formatImageUrl:(NSString *)url width:(int)w height:(int)h
{
    NSString * imageUpload = @"/image/upload/";
    NSArray * parts = [url componentsSeparatedByString:imageUpload];
    NSString * formatSize = [NSString stringWithFormat:@"w_%d,h_%d,c_limit/", w, h];
    
    return [NSString stringWithFormat:@"%@%@%@%@",[parts firstObject], imageUpload, formatSize, [parts lastObject]];
}

+(NSString *)formatFaceImageUrl:(NSString *)url width:(int)w height:(int)h
{
    NSString * imageUpload = @"/image/upload/";
    NSArray * parts = [url componentsSeparatedByString:imageUpload];
    NSString * formatSize = [NSString stringWithFormat:@"w_%d,h_%d,c_thumb,g_face/", w, h];
    
    return [NSString stringWithFormat:@"%@%@%@%@",[parts firstObject], imageUpload, formatSize, [parts lastObject]];
}

@end