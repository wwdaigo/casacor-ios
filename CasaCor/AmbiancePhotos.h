//
//  AmbiancePhotos.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 24/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface AmbiancePhotos : NSManagedObject

@property (nonatomic, retain) NSNumber * ambianceId;
@property (nonatomic, retain) NSNumber * oid;
@property (nonatomic, retain) NSString * photo;

@end
