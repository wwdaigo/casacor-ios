//
//  NewScrapViewController.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 25/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ambiances.h"
#import "RestConnection.h"
#import "PropertiesHelper.h"
#import "MBProgressHUD.h"

@interface NewScrapViewController : UIViewController <RestConnectionDelegate, UIAlertViewDelegate>
{
    IBOutlet UITextField * txtName;
    IBOutlet UITextField * txtEmail;
    IBOutlet UITextView * txtMessage;
    
    IBOutlet NSLayoutConstraint * bottomConstraint;
}

@property (nonatomic, strong) Ambiances * ambiance;

@end
