//
//  DAOAbstract.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 13/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "DAOAbstract.h"

@implementation DAOAbstract


-(id)init
{
    if (self = [super init])
    {
        AppDelegate * appDelegate = [[UIApplication sharedApplication] delegate];
        moc = [appDelegate managedObjectContext];
        
        entity = [NSEntityDescription entityForName:self.entityName inManagedObjectContext:moc];
        request = [NSFetchRequest new];
        [request setEntity:entity];
    }
    
    return self;
}

-(NSManagedObject *)objectByOID:(NSNumber *)oid
{
    if (oid == nil)
    {
        return [self newObjectWithOID:oid];
    }
    else
    {
        NSError * error;
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"oid == %@",oid];
        [request setPredicate:predicate];
        
        NSArray * res = [[NSArray alloc] initWithArray:[[moc executeFetchRequest:request error:&error] mutableCopy]];
        
        if ([res count]==0)
        {
            return [self newObjectWithOID:oid];
        }
        else {
            return [res objectAtIndex:0];
        }
    }
}

-(NSManagedObject *)newObjectWithOID:(NSNumber *)oid
{
    NSManagedObject * object = [NSEntityDescription insertNewObjectForEntityForName:self.entityName inManagedObjectContext:moc];
    
    [object setValue:oid forKey:@"oid"];

    return object;
}


-(void)save:(NSManagedObject *)dataObject
{
    NSError * error;
    [moc save:&error];
}

-(void)remove:(NSManagedObject *)dataObject
{
    NSError * error;
    [moc deleteObject:dataObject];
    [moc save:&error];
}

-(void)clear
{
    NSArray * list = [self list];
    [self clearWithObjects:list];
}

-(void)clearWithObjects:(NSArray *)objects
{
    NSError * error;
    
    @try
    {
        for (NSManagedObject * dataObject in objects)
        {
            [moc deleteObject:dataObject];
            [moc save:&error];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"%@",exception);
    }
    @finally
    {
        
    }
    
}

-(NSArray *)list
{
    return [self listOrderedBy:@"oid" ascending:YES];
}

-(NSArray *)listOrderedBy:(NSString *)field ascending:(BOOL)ascending
{
    NSError * error;
    NSSortDescriptor * order = [[NSSortDescriptor alloc] initWithKey:field ascending:ascending selector:@selector(compare:)];
    [request setSortDescriptors:[NSArray arrayWithObject:order]];
    return [[NSArray alloc] initWithArray:[[moc executeFetchRequest:request error:&error] mutableCopy]];
}

-(NSArray *)search:(NSString *)query
{
    return [self search:query orderedBy:@"oid" ascending:YES];
}


-(NSArray *)search:(NSString *)query orderedBy:(NSString *)field ascending:(BOOL)ascending
{
    NSError * error;
    NSPredicate * predicate = [NSPredicate predicateWithFormat:query];
    [request setPredicate:predicate];
    
    @try {
        NSSortDescriptor * order = [[NSSortDescriptor alloc] initWithKey:field ascending:ascending selector:@selector(compare:)];
        [request setSortDescriptors:[NSArray arrayWithObject:order]];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
    NSArray * res = [[NSArray alloc] initWithArray:[[moc executeFetchRequest:request error:&error] mutableCopy]];
    
    return res;
}

@end
