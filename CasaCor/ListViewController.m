//
//  ListViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 14/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "ListViewController.h"

@implementation ListViewController

-(void)viewDidLoad
{
    [super viewDidLoad];

}

-(void)startLoading
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)endLoading
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end
