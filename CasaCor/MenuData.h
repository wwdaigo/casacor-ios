//
//  MenuData.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 15/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuData : NSObject

@property (nonatomic, strong) UIImage * image;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * action;

@end
