//
//  AmbianceScrollView.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 15/09/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "AmbianceScrollView.h"

@implementation AmbianceScrollView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)loadProductsFromAmbiancePhotos:(AmbiancePhotos *)ambiancePhoto
{
    for (id view in self.subviews)
    {
        if ([view isKindOfClass:[UIImageView class]])
            imageView = view;
    }
    
    ambPhoto = ambiancePhoto;
    
    RestConnection * rest = [RestConnection new];
    [rest setDelegate:self];
    [rest getAmbiancesProdutctsWithID:ambiancePhoto.oid];
}


-(void)restConnection:(RestConnection *)restConnection succeededEntity:(NSString *)entity
{
    [self createButtons];
    //[self endLoading];
}

-(void)restConnection:(RestConnection *)restConnection failedEntity:(NSString *)entity error:(NSError *)error
{
    //    [self endLoading];
}

-(void)createButtons
{
    AmbianceProductsDAO * dao = [AmbianceProductsDAO new];
    NSArray * list = [dao search:[NSString stringWithFormat:@"ambiancePhotoId == %@", ambPhoto.oid]];
    
    CGRect frame = imageView.frame;
    
    CGSize size = frame.size;
    CGPoint point = frame.origin;
    
    for (AmbianceProducts * prod in list)
    {
        CGFloat posX = [prod.posX floatValue];
        CGFloat posY = [prod.posY floatValue];
        ProductButtonView * btn = [[ProductButtonView alloc] initWithFrame:CGRectMake(point.x+(size.width * posX)-22, point.y + (size.height * posY)-22, 44, 44)];
        [btn setProductID:prod.productId];
        [btn setPosX:prod.posX];
        [btn setPosY:prod.posY];

        [self addSubview:btn];
        
    }
}

@end
