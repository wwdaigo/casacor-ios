//
//  Ambiances.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 27/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Ambiances : NSManagedObject

@property (nonatomic, retain) NSNumber * editionId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * oid;
@property (nonatomic, retain) NSNumber * position;
@property (nonatomic, retain) NSString * quizHTML;
@property (nonatomic, retain) NSNumber * quiz;

@end
