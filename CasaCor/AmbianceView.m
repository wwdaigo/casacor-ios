//
//  AmbianceView.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 26/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "AmbianceView.h"

@implementation AmbianceView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];

    screenSize = [[UIScreen mainScreen] bounds].size;
    [self setFrame:CGRectMake(0, 0, screenSize.width, screenSize.height-64)];
    
    [self.scroll removeConstraint:[self.scroll.constraints lastObject]];
    [self.scroll removeConstraint:[self.scroll.constraints lastObject]];
    
    [self layoutIfNeeded];
    [self layoutSubviews];
}


-(CGFloat)minimumScale:(UIImage *)image
{
    if (image.size.width > image.size.height)
        return screenSize.width / image.size.width;
    else
        return screenSize.width / image.size.height;
}

-(void)setAmbiancePhoto:(AmbiancePhotos *)ambiancePhoto
{
    _ambiancePhoto = ambiancePhoto;
    
    [ImageHelper imageWithURLString:[self.ambiancePhoto photo] prefix:@"amb" oid:[self.ambiancePhoto oid] andCompletionHandler:^(UIImage *image) {
        
        [self.imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self.imageView setImage:image];
        [self.loading stopAnimating];
      
        
        [self.scroll setZoomScale:1 animated:YES];
        [self.scroll setMinimumZoomScale:[self minimumScale:image]];
        [self.scroll setMaximumZoomScale:1];
        
        CGFloat height = screenSize.height-64;
        CGFloat ratio = height / image.size.height;
        CGFloat width = image.size.width * ratio;
        
        [self.scroll setContentSize:CGSizeMake(screenSize.width, height)];
        
        
        NSLayoutConstraint * h = [NSLayoutConstraint constraintWithItem:self.imageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:height];
        NSLayoutConstraint * w = [NSLayoutConstraint constraintWithItem:self.imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:width];
        
        [self.imageView addConstraint:h];
        [self.imageView addConstraint:w];
        
        
    }];

    

   
    
    

    /*
    */
    
}
/*

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}
 */


/*
-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    NSLog(@"%@",view);
    CGFloat width = scrollView.contentSize.width/2;
    width =width-width/scrollView.zoomScale;
    
    CGFloat height = scrollView.contentSize.height/2;
    height =height-height/scrollView.zoomScale;
    
    scrollView.contentOffset = CGPointMake(width, height);
}*/
/*
-(void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    CGFloat width = scrollView.contentSize.width/2;
    width =width-width/scrollView.zoomScale;
    
    CGFloat height = scrollView.contentSize.height/2;
    height =height-height/scrollView.zoomScale;
    
    scrollView.contentOffset = CGPointMake(width, height);
}
*/

/*

*/

@end
