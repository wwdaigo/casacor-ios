//
//  WebViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 14/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "WebViewController.h"

@implementation WebViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self webView] setDelegate:self];
}

-(void)openURL:(NSURL *)url
{
    theURL = url;

    req = [[NSURLRequest alloc] initWithURL:url];

    [self.webView loadRequest:req];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)loadHTML:(NSString *)html
{
    [self.webView loadHTMLString:html baseURL:nil];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

#pragma mark - Web View Delegate

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"Erro" message:error.debugDescription delegate:self cancelButtonTitle:@"Fechar" otherButtonTitles:nil] show];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request  navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"%s",__PRETTY_FUNCTION__);
    
    if (!_Authenticated)
    {
        _Authenticated = NO;
        
        _urlConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        [_urlConnection start];
        
        return NO;
    }
    
    return YES;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;
{
    NSLog(@"WebController Got auth challange via NSURLConnection");
    
    if ([challenge previousFailureCount] == 0)
    {
        NSLog(@"SIM");
        _Authenticated = YES;
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        
        [challenge.sender useCredential:credential forAuthenticationChallenge:challenge];
        
    } else
    {
        NSLog(@"NAO");
        [[challenge sender] cancelAuthenticationChallenge:challenge];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;
{
    NSLog(@"WebController received response via NSURLConnection");
    
    // remake a webview call now that authentication has passed ok.
    _Authenticated = YES;
    [_webView loadRequest:req];
    
    // Cancel the URL connection otherwise we double up (webview + url connection, same url = no good!)
    [_urlConnection cancel];
}

// We use this method is to accept an untrusted site which unfortunately we need to do, as our PVM servers are self signed.
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

@end
