//
//  EditionsTableDataSource.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 16/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "EditionsTableDataSource.h"

@implementation EditionsTableDataSource

-(id)init
{
    if (self = [super init])
    {
        EditionsDAO * dao = [EditionsDAO new];
        [super setList:[dao list]];
    }
    return self;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[super list] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EditionCell" forIndexPath:indexPath];
    
    Editions * data = [[super list] objectAtIndex:indexPath.row];
    
    UILabel * lblName = (UILabel *)[cell viewWithTag:1];
    UILabel * lblLocation = (UILabel *)[cell viewWithTag:2];
    
    [lblName setText:[data.name uppercaseString]];
    [lblLocation setText:data.location];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pickEdition" object:[[super list] objectAtIndex:indexPath.row]];
}

@end
