//
//  MainListViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 14/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "MainListViewController.h"

@implementation MainListViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    filteredList = [NSMutableArray new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openMenuOption:) name:@"menuOption" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openEditionPicker:) name:@"openEditionPicker" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pickEdition:) name:@"pickEdition" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openProfessional:) name:@"openProfessional" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openAmbience:) name:@"openAmbience" object:nil];
   
    
    
    
    searchButton = self.navigationItem.rightBarButtonItem;
    
    [self setMainTitle];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardNotification:) name:UIKeyboardDidChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self performSelector:@selector(getEvents) withObject:nil afterDelay:3];
}


-(void)getEvents
{
    RestConnection * rest = [RestConnection new];
    [rest setDelegate:self];
    
    [rest getEvents];

}


-(void)setMainTitle
{
    mainTitleView = [[[NSBundle mainBundle] loadNibNamed:@"MainTitleView" owner:self options:nil] objectAtIndex:0];
    self.navigationItem.titleView = mainTitleView;
    
    [self.navigationItem setRightBarButtonItem:searchButton];
    [self setupEdition];
}

-(void)setSearchTitle
{
    searchTitleView = [[[NSBundle mainBundle] loadNibNamed:@"SearchTitleView" owner:self options:nil] objectAtIndex:0];
    self.navigationItem.titleView = searchTitleView;
    
    [[searchTitleView searchBar] setDelegate:self];
    [[searchTitleView searchBar] becomeFirstResponder];
    
    [self.navigationItem setRightBarButtonItem:nil];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!sponsorDisplayed)
    {
        sponsorDisplayed = YES;
        [self performSegueWithIdentifier:@"sponsor" sender:nil];
    }
    
}


-(void)setupEdition
{
    EditionsDAO * dao = [EditionsDAO new];
    Editions * edition = [dao objectByOID:[PropertiesHelper chosenEdition]];
 
    [mainTitleView setCity:[edition.city uppercaseString]];

    [[super tableView] reloadData];
}

-(IBAction)toggleMenu:(id)sender
{
    AppDelegate * appDelegate = [[UIApplication sharedApplication] delegate];
    [[appDelegate drawerContainer] toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}


-(void)openMenuOption:(NSNotification *)notification
{
    if ([notification.object isEqualToString:@"map"])
    {
        [self performSegueWithIdentifier:@"goMap" sender:nil];
    }
    else if ([notification.object isEqualToString:@"ticket"])
    {
        [self performSegueWithIdentifier:@"goTicket" sender:nil];
    }
    else if ([notification.object isEqualToString:@"taxi"])
    {
        [self performSegueWithIdentifier:@"goTaxi" sender:nil];
    }
    else if ([notification.object isEqualToString:@"quiz"])
    {
        [self performSegueWithIdentifier:@"goQuiz" sender:nil];
    }
    
    [self toggleMenu:nil];
}

-(void)openEditionPicker:(NSNotification *)notification
{
    [self performSegueWithIdentifier:@"changeEdition" sender:nil];
}

-(void)pickEdition:(NSNotification *)notification
{
//    [super startLoading];
    
    Editions * edition = (Editions *)notification.object;
    [PropertiesHelper setChosenEdition:edition.oid];

    [self loadList];
    
    
    [self loadDataFromCurrentEvent];
}

-(void)openProfessional:(id)sender
{
    [self performSegueWithIdentifier:@"showProfessional" sender:sender];
}

-(void)openAmbience:(id)sender
{
    UITableViewCell * cell = (UITableViewCell *)[[sender superview] superview];
    UITableView * tableView = (UITableView *)[[cell superview] superview];
    NSIndexPath * indexPath = [tableView indexPathForCell:cell];
    
    Ambiances * ambiance = [[self rightList] objectAtIndex:indexPath.row];
    
    AmbiancePhotosDAO * photosDAO = [AmbiancePhotosDAO new];
    NSArray * listPhoto = [photosDAO search:[NSString stringWithFormat:@"ambianceId==%@",ambiance.oid]];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"NoAutoLayout" bundle:nil];

    AmbianceDetailsViewController * detailsViewcontroller = [storyBoard instantiateViewControllerWithIdentifier:@"AmbianceDetailsViewController"];
    [detailsViewcontroller setAmbiancePhotos:listPhoto];
    [self showViewController:detailsViewcontroller sender:sender];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showProfessional"])
    {
        ProfessionalViewController * dest = [segue destinationViewController];
        [dest setDataObject:sender];
    }
    
}

-(void)loadDataFromCurrentEvent
{
    if ([PropertiesHelper chosenEdition] == nil)
    {
        [self performSegueWithIdentifier:@"changeEdition" sender:nil];
    }
    else
    {
        [super startLoading];
        
        [self setupEdition];
        
        RestConnection * rest = [RestConnection new];
        [rest setDelegate:self];
        [rest getAmbiancesWithEventID:[PropertiesHelper chosenEdition]];
    }
}



#pragma mark - RestConn delegate


-(void)restConnection:(RestConnection *)restConnection succeededEntity:(NSString *)entity
{
    if ([entity isEqualToString:@"Events"])
    {
        [self loadDataFromCurrentEvent];
    }
    else if ([entity isEqualToString:@"Ambiances"])
    {
        [super endLoading];
        [self loadList];
    }
}

-(void)restConnection:(RestConnection *)restConnection failedEntity:(NSString *)entity error:(NSError *)error
{
    [super endLoading];
}






-(void)loadList
{
    AmbiancesDAO * dao = [AmbiancesDAO new];
    list = [[NSMutableArray alloc] initWithArray:[dao search:[NSString stringWithFormat:@"editionId == %@", [PropertiesHelper chosenEdition]] orderedBy:@"position" ascending:YES]];

    [[super tableView] reloadData];
 
    
}

-(NSArray *)rightList
{
    if (isSearching)
        return filteredList;
    else
        return list;

}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self rightList] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    Ambiances * ambiance = [[self rightList] objectAtIndex:indexPath.row];
    
    UILabel * lblAmbience = (UILabel *)[cell viewWithTag:1];
    UIButton * btnProfessional = (UIButton *)[cell viewWithTag:2];
    UIImageView * imgAmbience = (UIImageView *)[cell viewWithTag:3];
    
    UIButton * btnAmbience = (UIButton *)[cell viewWithTag:5];
    [btnAmbience addTarget:self action:@selector(openAmbience:) forControlEvents:UIControlEventTouchUpInside];
    
    UICollectionView * professionalCollectionView  = (UICollectionView *)[cell viewWithTag:6];
    [professionalCollectionView setDataSource:self];
    [professionalCollectionView setDelegate:self];
    [professionalCollectionView reloadData];
    
    [btnProfessional addTarget:self action:@selector(openProfessional:) forControlEvents:UIControlEventTouchUpInside];
    
    UIActivityIndicatorView * loading  = (UIActivityIndicatorView *)[cell viewWithTag:7];
    [loading startAnimating];
    
    
    [lblAmbience setText:[ambiance.name uppercaseString]];
    
    
   
    AmbiancePhotosDAO * photosDAO = [AmbiancePhotosDAO new];
    NSArray * listPhoto = [photosDAO search:[NSString stringWithFormat:@"ambianceId==%@",ambiance.oid]];
    
    if (listPhoto.count > 0)
    {
        AmbiancePhotos * photo = [listPhoto firstObject];
        BOOL has = [ImageHelper imageWithURLString:[CloudinaryUtils formatImageUrl:[photo photo] width:600 height:400] prefix:@"amb" oid:[photo oid] andCompletionHandler:^(UIImage *image)
        {
            NSLog(@"%@",[CloudinaryUtils formatImageUrl:[photo photo] width:600 height:400]);
            [imgAmbience setImage:image];
        }];
        
        if (!has) [imgAmbience setImage:nil];
    }
    else
        [imgAmbience setImage:[UIImage imageNamed:@"placeholder.jpg"]];
    
    return cell;
    
}

-(NSArray *)professionaListAtIndexPath:(NSIndexPath *)indexPath
{
    Ambiances * ambiance = [list objectAtIndex:indexPath.row];
    
    AmbianceProfessionalDAO * ambianceProfDAO = [AmbianceProfessionalDAO new];
    NSArray * ambianceProfList = [ambianceProfDAO search:[NSString stringWithFormat:@"ambianceId == %@",ambiance.oid]];
    NSMutableArray * tempArray = [NSMutableArray new];
    
    for (AmbianceProfessional * pro in ambianceProfList)
    {
        ProfessionalsDAO * profDAO = [ProfessionalsDAO new];
        Professionals * professional = (Professionals *)[profDAO objectByOID:pro.professionalId];
        
        [tempArray addObject:professional];
    }
    
    
    return [NSArray arrayWithArray:tempArray];
}

-(NSIndexPath *)indexPathFromCollectionView:(UICollectionView *)collectionView
{
    UITableViewCell * cell = (UITableViewCell *)[[collectionView superview] superview];
    NSIndexPath * index = [[super tableView] indexPathForCell:cell];
    
    return index;
}

#pragma mark - UICollectionView Datasource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSLog(@"looo %@",[self professionaListAtIndexPath:[self indexPathFromCollectionView:collectionView]]);
    return [[self professionaListAtIndexPath:[self indexPathFromCollectionView:collectionView]] count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Professional" forIndexPath:indexPath];
    
    UIImageView * imageView  = (UIImageView *)[cell viewWithTag:8];
    UIActivityIndicatorView * loading  = (UIActivityIndicatorView *)[cell viewWithTag:9];
    
    [loading startAnimating];
    
    NSIndexPath * ambianceIndexPath = [self indexPathFromCollectionView:collectionView];
    NSArray * professionals = [self professionaListAtIndexPath:ambianceIndexPath];
    
    Professionals * professional = [professionals objectAtIndex:indexPath.row];
    
    if (![[professional photo] isEqualToString:@""])
    {
        NSLog(@"[professional photo] %@",[professional photo]);
        NSLog(@"[professional oid] %@",[professional oid]);
        BOOL has = [ImageHelper imageWithURLString:[CloudinaryUtils formatFaceImageUrl:[professional photo] width:40 height:40] prefix:@"pro" oid:[professional oid] andCompletionHandler:^(UIImage *image) {
            [imageView setImage:image];
            NSLog(@"image %@",image);
        }];
        
        if (!has) [imageView setImage:nil];
    }
    else
    {
        [imageView setImage:[UIImage imageNamed:@"user_placeholder"]];
    }
    
    
    return cell;
}

#pragma mark - UICollectionView Delegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath * ambianceIndexPath = [self indexPathFromCollectionView:collectionView];
    NSArray * professionals = [self professionaListAtIndexPath:ambianceIndexPath];
    Professionals * professional = [professionals objectAtIndex:indexPath.row];
    
    [self openProfessional:professional];
}

-(IBAction)toggleSearchMode:(id)sender
{
    [self setSearchTitle];
}


-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    isSearching = NO;
    [self setMainTitle];
}


- (void)handleKeyboardNotification:(NSNotification *)aNotification
{
    NSDictionary* keyboardInfo = [aNotification userInfo];
    NSValue* keyboardFrame = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameRect = [keyboardFrame CGRectValue];
    
    [self setBottom:keyboardFrameRect.size.height];
}

- (void)handleKeyboardHide:(NSNotification *)aNotification
{
    [self setBottom:0];
}

-(void)setBottom:(float)constant
{
    [UIView animateWithDuration:0.1 animations:^{
        bottomConstraint.constant = constant;
        
        [self.view layoutIfNeeded];
        [self.view layoutSubviews];
        
    } completion:^(BOOL finished) {

    }];
}



-(void)filterContentForSearchText:(NSString*)searchText
{
    [filteredList removeAllObjects];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[c] %@",searchText];
    filteredList = [NSMutableArray arrayWithArray:[list filteredArrayUsingPredicate:predicate]];
    
    [[super tableView] reloadData];
}

#pragma mark - UISearchDisplayController Delegate Methods

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText isEqualToString:@""])
        isSearching = NO;
    else
        isSearching = YES;
        
    [self filterContentForSearchText:searchText];
}

/*
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {

    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];

    
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    return YES;
}*/



@end
