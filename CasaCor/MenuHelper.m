//
//  MenuHelper.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 15/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "MenuHelper.h"

@implementation MenuHelper



-(NSArray *)list
{
    NSArray * listMenu = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Menu" ofType:@"plist"]];
    NSMutableArray * arrList = [NSMutableArray new];
    
    for (NSDictionary * dict in listMenu)
    {
        MenuData * menu = [MenuData new];
        
        [menu setTitle:[dict objectForKey:@"title"]];
        [menu setAction:[dict objectForKey:@"action"]];
        [menu setImage:[UIImage imageNamed:[dict objectForKey:@"image"]]];
        
        [arrList addObject:menu];
    }
    
    return [NSArray arrayWithArray:arrList];
}


@end
