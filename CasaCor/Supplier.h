//
//  Supplier.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 24/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Supplier : NSManagedObject

@property (nonatomic, retain) NSNumber * isPremium;
@property (nonatomic, retain) NSString * logo;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * oid;

@end
