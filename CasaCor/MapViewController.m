//
//  MapViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 27/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.'
    
    EditionsDAO * dao = [EditionsDAO new];
    Editions * edition = [dao objectByOID:[PropertiesHelper chosenEdition]];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [ImageHelper imageWithURLString:edition.map prefix:@"map" oid:edition.oid andCompletionHandler:^(UIImage *image) {
        [self.mapImage setImage:image];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
