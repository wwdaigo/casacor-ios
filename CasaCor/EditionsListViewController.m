//
//  EditionsListViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 16/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "EditionsListViewController.h"

@interface EditionsListViewController ()

@end

@implementation EditionsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pickEdition:) name:@"pickEdition" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)pickEdition:(NSNotification *)notification
{
    [self close:nil];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}






@end
