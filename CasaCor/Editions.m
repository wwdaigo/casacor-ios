//
//  Editions.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 27/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "Editions.h"


@implementation Editions

@dynamic city;
@dynamic latitude;
@dynamic location;
@dynamic longitude;
@dynamic name;
@dynamic oid;
@dynamic state;
@dynamic ticketUrl;
@dynamic votingUrl;
@dynamic map;

@end
