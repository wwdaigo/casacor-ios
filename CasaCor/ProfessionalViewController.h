//
//  ProfessionalViewController.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 17/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ProfessionalsDAO.h"
#import "RestConnection.h"
#import "MBProgressHUD.h"

@interface ProfessionalViewController : UIViewController <RestConnectionDelegate>

@property (nonatomic, strong) IBOutlet UIImageView * imgPhoto;
@property (nonatomic, strong) IBOutlet UILabel * lblName;
@property (nonatomic, strong) IBOutlet UILabel * lblEmail;
@property (nonatomic, strong) IBOutlet UILabel * lblWebsite;

@property (nonatomic, strong) IBOutlet UIImageView * imgEmail;
@property (nonatomic, strong) IBOutlet UIImageView * imgWeb;

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView * loading;

@property (nonatomic, strong) Professionals * dataObject;

@end
