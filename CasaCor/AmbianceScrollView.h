//
//  AmbianceScrollView.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 15/09/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmbiancePhotosDAO.h"
#import "AmbianceProductsDAO.h"
#import "RestConnection.h"
#import "ProductButtonView.h"

@interface AmbianceScrollView : UIScrollView <RestConnectionDelegate>
{
    AmbiancePhotos * ambPhoto;
    UIImageView * imageView;
}

-(void)loadProductsFromAmbiancePhotos:(AmbiancePhotos *)ambiancePhoto;

@end
