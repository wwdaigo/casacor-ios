//
//  PhotosViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 17/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "PhotosViewController.h"

@interface PhotosViewController ()

@end

@implementation PhotosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self reloadImages];
    [[self scrollView] setPagingEnabled:YES];
    [[self scrollView] setDelegate:self];
    
    [[self pageControl] setUserInteractionEnabled:NO];
}

-(void)reloadImages
{
    [self clear];
//    CGSize sSize = [[UIScreen mainScreen] bounds].size;
    
    if (self.views.count > 0)
    {
//        NSDictionary * metrics = @{@"width":[NSNumber numberWithFloat:sSize.width], @"height":[NSNumber numberWithFloat:sSize.height-64]};
        NSMutableDictionary * dictViews = [NSMutableDictionary new];
        
        NSString * alignH = @"";
        NSArray*colors =  @[[UIColor redColor], [UIColor blueColor], [UIColor yellowColor], [UIColor blackColor], [UIColor grayColor], [UIColor purpleColor]];
        int i=0;
        for (UIView * view in self.views)
        {
            NSString * key = [NSString stringWithFormat:@"view%d",i];
            [dictViews setObject:view forKey:key];
            
            alignH = [NSString stringWithFormat:@"%@[%@]-0-",alignH, key];
            
            [view setTranslatesAutoresizingMaskIntoConstraints:NO];
            [self.scrollView addSubview:view];
            [view setBackgroundColor:[colors objectAtIndex:i]];
            
            
            NSLayoutConstraint * width = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1 constant:0];
            NSLayoutConstraint * height = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeHeight multiplier:1 constant:-64];
            
            [self.view addConstraint:width];
            [self.scrollView addConstraint:height];
            
            NSArray * constV = [NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-0-[%@]-0-|", key] options:0 metrics:nil views:dictViews];
            [self.scrollView addConstraints:constV];
            
            i++;
        }
        
        NSString * strHor = [NSString stringWithFormat:@"H:|-0-%@|", alignH];
        NSArray * constH = [NSLayoutConstraint constraintsWithVisualFormat:strHor options:0 metrics:nil views:dictViews];
        
        [self.scrollView addConstraints:constH];
    }
    
    
    [self.pageControl setNumberOfPages:self.views.count];
}

-(void)clear
{
    for (UIView * view in [self.scrollView subviews])
    {
        [view removeFromSuperview];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)startLoading
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)endLoading
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger index = scrollView.contentOffset.x / scrollView.frame.size.width;
    [self.pageControl setCurrentPage:index];
}

@end
