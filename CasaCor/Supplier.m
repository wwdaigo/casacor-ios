//
//  Supplier.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 24/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "Supplier.h"


@implementation Supplier

@dynamic isPremium;
@dynamic logo;
@dynamic name;
@dynamic oid;

@end
