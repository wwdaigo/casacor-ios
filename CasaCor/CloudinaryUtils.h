//
//  CloudinaryUtils.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 04/11/15.
//  Copyright © 2015 Muuving. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CloudinaryUtils : NSObject

+(NSString *)formatImageUrl:(NSString *)url width:(int)w height:(int)h;
+(NSString *)formatFaceImageUrl:(NSString *)url width:(int)w height:(int)h;

@end
