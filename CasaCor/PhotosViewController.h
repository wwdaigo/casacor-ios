//
//  PhotosViewController.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 17/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface PhotosViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) NSArray * views;

@property (nonatomic, strong) IBOutlet UIScrollView * scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl * pageControl;

-(void)startLoading;
-(void)endLoading;


@end
