//
//  MenuHelper.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 15/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MenuData.h"

@interface MenuHelper : NSObject

-(NSArray *)list;

@end
