//
//  AmbianceView.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 26/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmbiancePhotos.h"
#import "RestConnection.h"
#import "AmbianceProductsDAO.h"
#import "ImageHelper.h"
#import "ProductButtonView.h"

@interface AmbianceView : UIView <RestConnectionDelegate, UIScrollViewDelegate>
{
    CGSize screenSize;
}
@property (nonatomic, strong) IBOutlet UIScrollView * scroll;

@property (nonatomic, strong) IBOutlet UIImageView * imageView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView * loading;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint * w;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint * h;

@property (nonatomic, strong) AmbiancePhotos * ambiancePhoto;

@end
