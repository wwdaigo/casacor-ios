//
//  AmbianceDetailsViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 14/09/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "AmbianceDetailsViewController.h"
#import "ProductViewController.h"
#import "AmbianceQuizViewController.h"

@interface AmbianceDetailsViewController ()

@end

@implementation AmbianceDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goProduct:) name:@"productButtonTapped" object:nil];
    
    [self setTitle:@"Ambiente"];
    screenSize = [[UIScreen mainScreen] bounds].size;
    
    NSInteger pageCount = self.ambiancePhotos.count;
    
    // Set up the array to hold the views for each page
    self.pageViews = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < pageCount; ++i) {
        [self.pageViews addObject:[NSNull null]];
    }
    
    if ([ambiance.quiz isEqualToNumber:[NSNumber numberWithBool:NO]] || ambiance.quiz == nil)
    {
        [self.btnQuiz setTitle:@" "];
        [self.btnQuiz setEnabled:NO];
    }
    else
    {
        if (![PropertiesHelper quizOpened:ambiance.oid])
        {
            [PropertiesHelper setQuizOpened:ambiance.oid];
            
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            AmbianceQuizViewController * dest = [storyBoard instantiateViewControllerWithIdentifier:@"AmbianceQuizViewController"];;
            [dest setAmbiance:ambiance];
            [self presentViewController:dest animated:YES completion:nil];
        }
        
    }


    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Set up the content size of the scroll view
    CGSize pagesScrollViewSize = self.scrollView.frame.size;
    self.scrollView.contentSize = CGSizeMake(pagesScrollViewSize.width * self.ambiancePhotos.count, pagesScrollViewSize.height-64);
    
    // Load the initial set of pages that are on screen
    [self loadVisiblePages];
}

- (void)loadVisiblePages
{
    for (NSInteger i=0; i<=self.ambiancePhotos.count; i++)
    {
        [self loadPage:i];
    }
}

- (void)loadPage:(NSInteger)page
{
    if (page < 0 || page >= self.ambiancePhotos.count) {
        // If it's outside the range of what we have to display, then do nothing
        return;
    }
    
    // Load an individual page, first seeing if we've already loaded it
    UIView *pageView = [self.pageViews objectAtIndex:page];
    if ((NSNull*)pageView == [NSNull null])
    {
        AmbiancePhotos * amb = [self.ambiancePhotos objectAtIndex:page];
        
        
        CGRect frame = self.scrollView.bounds;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0.0f;
        frame.size.height -= 64;
        
        
        AmbianceScrollView * scroll = [[AmbianceScrollView alloc] initWithFrame:frame];
        [scroll setClipsToBounds:YES];
        
        UIImageView *newPageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [scroll addSubview:newPageView];
        
        newPageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [ImageHelper imageWithURLString:[amb photo] prefix:@"amb" oid:[amb oid] andCompletionHandler:^(UIImage *image) {
            
            CGFloat ratio = [self minimumScale:image];
            [newPageView setImage:image];
            [newPageView setFrame:CGRectMake(0, 0, image.size.width*ratio, image.size.height*ratio)];
            
            
            
            CGFloat maxZoom = scroll.frame.size.height / newPageView.frame.size.height;

            [scroll setZoomScale:1 animated:YES];
            [scroll setMinimumZoomScale:1];
            [scroll setMaximumZoomScale:maxZoom];
            
        
            [self centerScrollViewContents:scroll imageView:newPageView];
            
            [scroll loadProductsFromAmbiancePhotos:amb];
        }];
        
        [scroll setDelegate:self];
        

        [self.scrollView addSubview:scroll];
        
        [self.pageViews replaceObjectAtIndex:page withObject:scroll];
    }
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    UIImageView * imageView = [self imageViewInScrollView:scrollView];
    return imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIImageView * imageView = [self imageViewInScrollView:scrollView];
    [self centerScrollViewContents:scrollView imageView:imageView];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
  //  [self centerScrollViewContents:scrollView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)centerScrollViewContents:(UIScrollView *)scrollView imageView:(UIImageView *)imageView
{
    CGSize boundsSize = scrollView.bounds.size;
    CGRect contentsFrame = imageView.frame;
    
    contentsFrame.origin.x = 0;
    contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    
    imageView.frame = contentsFrame;
    
    [self relocateButtonsFromScrollView:scrollView];
}

-(void)relocateButtonsFromScrollView:(UIScrollView *) scroll
{
    UIImageView *imageView = [self imageViewInScrollView:scroll];
    
    for (id btnView in scroll.subviews)
    {
        if ([btnView isKindOfClass:[ProductButtonView class]])
        {
            [btnView adjustPositionWithSize:imageView.frame];
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(CGFloat)minimumScale:(UIImage *)image
{
    CGFloat w = self.scrollView.frame.size.width / image.size.width;
    CGFloat h = self.scrollView.frame.size.height / image.size.height;
    
    return MIN(w, h);
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    UIImageView * imageView = [self imageViewInScrollView:scrollView];
    CGFloat width = scrollView.frame.size.width;
    
    CGFloat imageOffset = imageView.frame.size.width-scrollView.frame.size.width;

    CGFloat rightOffset = scrollView.contentOffset.x-imageOffset;

    
    if (rightOffset > 60 && self.scrollView.contentOffset.x < self.scrollView.contentSize.width-width)
    {
        [self.scrollView setContentOffset:CGPointMake(width+self.scrollView.contentOffset.x, self.scrollView.contentOffset.y) animated:YES];
    }
    else if (scrollView.contentOffset.x < -60 && self.scrollView.contentOffset.x>0)
    {
        [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x-width, self.scrollView.contentOffset.y) animated:YES];
    }
    
    if (self.scrollView.contentOffset.x < 0)
        [self.scrollView setContentOffset:CGPointMake(0, self.scrollView.contentOffset.y) animated:YES];
}

-(UIImageView *)imageViewInScrollView:(UIScrollView *)scroll
{
    for (id view in scroll.subviews)
    {
        if ([view isKindOfClass:[UIImageView class]])
            return view;
    }
    
    return nil;
}
-(void)goProduct:(NSNotification *)notification
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NSNumber * ambianceProductOID = (NSNumber *)notification.object;
    ProductViewController * dest = [storyBoard instantiateViewControllerWithIdentifier:@"ProductViewController"];
    [dest setProductId:ambianceProductOID];
    
    [self showViewController:dest sender:nil];
}

-(IBAction)openScrapbook:(id)sender
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NewScrapViewController * dest = [storyBoard instantiateViewControllerWithIdentifier:@"NewScrapViewController"];;
    [dest setAmbiance:ambiance];
    
    [self showViewController:dest sender:nil];
}



@end
