//
//  RestConnection.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 21/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <Foundation/Foundation.h>


#define TOKEN @"Token token=xCCEPFzQZQxEeSGhjRXs"
#define RESTURL @"http://api.muuving.com.br/mobile/v1/"

@class RestConnection;
@protocol RestConnectionDelegate <NSObject>

-(void)restConnection:(RestConnection *)restConnection succeededEntity:(NSString *)entity;
-(void)restConnection:(RestConnection *)restConnection failedEntity:(NSString *)entity error:(NSError *)error;

@end

@interface RestConnection : NSObject <NSURLSessionDelegate>
{
    NSURLSession * urlSession;

}

@property (nonatomic, strong) id<RestConnectionDelegate> delegate;

-(void)getEvents;
-(void)getAmbiancesWithEventID:(NSNumber *)eventOID;
-(void)getAmbiancesProdutctsWithID:(NSNumber *)ambianceOID;
-(void)getProfessionalWithId:(NSNumber *)oid;
-(void)getProductWithId:(NSNumber *)oid;

-(void)postMessageWithName:(NSString *)name email:(NSString *)email ambianceId:(NSNumber *)ambianceId message:(NSString *)message;

@end
