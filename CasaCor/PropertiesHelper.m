//
//  PropertiesHelper.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 17/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "PropertiesHelper.h"

@implementation PropertiesHelper

+(void)setChosenEdition:(NSNumber *)oid
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:oid forKey:@"editionId"];
    
    [defaults synchronize];
}

+(NSNumber *)chosenEdition
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"editionId"];
}


+(void)setQuizOpened:(NSNumber *)oid
{
    NSString * key = [NSString stringWithFormat:@"ambiance%@",oid];
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@1 forKey:key];
    
    [defaults synchronize];
}

+(BOOL)quizOpened:(NSNumber *)oid
{
    NSString * key = [NSString stringWithFormat:@"ambiance%@",oid];
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    if ([defaults objectForKey:key] != nil)
        return YES;
    else
        return NO;
}

+(void)setName:(NSString *)name
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:name forKey:@"name"];
    
    [defaults synchronize];
}
+(NSString *)name
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"name"];
}

+(void)setEmail:(NSString *)email
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:email forKey:@"email"];
    
    [defaults synchronize];
}
+(NSString *)email
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"email"];
}


@end
