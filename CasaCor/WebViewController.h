//
//  WebViewController.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 14/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBProgressHUD.h"

@interface WebViewController : UIViewController <UIWebViewDelegate, NSURLConnectionDelegate>
{
    NSURL * theURL;
    
    BOOL _Authenticated;
    NSURLRequest *_FailedRequest;
    NSURLRequest * req;
}

@property (nonatomic, strong) IBOutlet UIWebView * webView;
@property (nonatomic, strong) NSURLConnection * urlConnection;


-(void)openURL:(NSURL *)url;
-(void)loadHTML:(NSString *)html;

@end
