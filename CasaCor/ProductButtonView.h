//
//  ProductButtonView.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 27/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AmbianceProducts.h"

@interface ProductButtonView : UIView
{
    BOOL created;
}
@property (nonatomic, strong) UIImageView * imageView;
@property (nonatomic, strong) UIButton * button;


@property (nonatomic, strong) NSNumber * productID;
@property (nonatomic, strong) NSNumber * posX;
@property (nonatomic, strong) NSNumber * posY;


-(IBAction)tap:(id)sender;
-(void)adjustPositionWithSize:(CGRect)rect;

@end
