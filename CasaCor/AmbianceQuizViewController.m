//
//  AmbianceQuizViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 27/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "AmbianceQuizViewController.h"

@interface AmbianceQuizViewController ()

@end

@implementation AmbianceQuizViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [super loadHTML:self.ambiance.quizHTML];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
