//
//  ImageHelper.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 26/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageHelper : NSObject
{
    
}

+(BOOL)imageWithURLString:(NSString *)urlstring prefix:(NSString *)prefix oid:(NSNumber *)oid andCompletionHandler:(void(^)(UIImage *image))completionHandler;

@end
