//
//  AmbiencesDAO.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 13/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "DAOAbstract.h"
#import "Ambiances.h"

@interface AmbiancesDAO : DAOAbstract

-(Ambiances *)objectByOID:(NSNumber *)oid;

@end
