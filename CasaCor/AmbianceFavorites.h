//
//  AmbianceFavorites.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 24/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface AmbianceFavorites : NSManagedObject

@property (nonatomic, retain) NSNumber * ambianceId;

@end
