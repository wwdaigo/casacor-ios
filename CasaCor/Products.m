//
//  Products.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 25/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "Products.h"


@implementation Products

@dynamic name;
@dynamic oid;
@dynamic price;
@dynamic segment;
@dynamic supplierId;
@dynamic photo;

@end
