//
//  EditionsDAO.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 13/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "EditionsDAO.h"

#import "Editions.h"

@implementation EditionsDAO

-(id)init
{
    [super setEntityName:@"Editions"];
    
    if (self = [super init])
    {

    }
    
    return self;
}


-(Editions *)objectByOID:(NSNumber *)oid
{
    Editions * object = (Editions *)[super objectByOID:oid];
    return object;
}
@end
