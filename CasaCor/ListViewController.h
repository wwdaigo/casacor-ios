//
//  ListViewController.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 14/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBProgressHUD.h"

@interface ListViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView * tableView;

-(void)startLoading;
-(void)endLoading;

@end
