//
//  MapViewController.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 27/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ImageHelper.h"
#import "PropertiesHelper.h"
#import "EditionsDAO.h"

@interface MapViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIImageView * mapImage;

@end
