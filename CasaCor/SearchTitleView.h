//
//  SearchTitleView.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 03/09/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTitleView : UIView

@property (nonatomic, strong) IBOutlet UISearchBar * searchBar;

@end
