//
//  DAOProtocol.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 13/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <CoreData/CoreData.h>

#ifndef CasaCor_DAOProtocol_h
#define CasaCor_DAOProtocol_h

@protocol DAOProtocol <NSObject>

-(void)save:(NSManagedObject *)dataObject;
-(void)remove:(NSManagedObject *)dataObject;

-(NSArray *)list;
-(NSArray *)listOrderedBy:(NSString *)field ascending:(BOOL)ascending;
-(NSArray *)search:(NSString *)query;
-(NSArray *)search:(NSString *)query orderedBy:(NSString *)field ascending:(BOOL)ascending;

-(void)clear;
-(void)clearWithObjects:(NSArray *)objects;

@end

#endif
