//
//  ProfessionalsDAO.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 13/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "ProfessionalsDAO.h"

@implementation ProfessionalsDAO

-(id)init
{
    [super setEntityName:@"Professionals"];
    
    if (self = [super init])
    {
        
    }
    
    return self;
}

-(Professionals *)objectByOID:(NSNumber *)oid
{
    Professionals * object = (Professionals *)[super objectByOID:oid];
    return object;
}

@end
