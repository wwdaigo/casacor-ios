//
//  TicketWebViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 14/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "TicketWebViewController.h"

#import "EditionsDAO.h"
#import "PropertiesHelper.h"

@implementation TicketWebViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
     EditionsDAO * dao = [EditionsDAO new];
     Editions * edition = [dao objectByOID:[PropertiesHelper chosenEdition]];
     
     NSLog(@"%@",edition.ticketUrl);
    [self openURL:[NSURL URLWithString:edition.ticketUrl]];
}

@end
