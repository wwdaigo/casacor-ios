//
//  DAOAbstract.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 13/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

#import "DAOProtocol.h"

@interface DAOAbstract : NSObject <DAOProtocol>
{
    NSManagedObjectContext * moc;
    NSEntityDescription *entity;
    NSFetchRequest * request;
}

-(NSManagedObject *)objectByOID:(NSNumber *)oid;

@property (nonatomic, strong) NSString * entityName;

@end
