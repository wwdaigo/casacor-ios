//
//  ProductViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 25/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "ProductViewController.h"

@interface ProductViewController ()

@end

@implementation ProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    RestConnection * rest = [RestConnection new];
    [rest setDelegate:self];
    
    [rest getProductWithId:self.productId];
    
    
    [self displayInfo];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"showPhoto"])
    {
        if (self.imageView.image != nil)
        {
            ProductsPhotosViewController * dest = [segue destinationViewController];
            [dest setImg:self.imageView.image];
        }
    }
}


-(void)displayInfo
{
    ProductsDAO * dao = [ProductsDAO new];
    product = [dao objectByOID:self.productId];
    
    [self.lblTitle setText:[product name]];
    [self.lblSegment setText:[product segment]];
    [self.lblPrice setText:@"R$ 0,00"];
    
    SupplierDAO * supDao = [SupplierDAO new];
    Supplier * supplier = [supDao objectByOID:product.supplierId];
    [self.lblSupplier setText:supplier.name];
    
    [ImageHelper imageWithURLString:[product photo] prefix:@"prod" oid:[product oid] andCompletionHandler:^(UIImage *image) {
        [self.imageView setImage:image];
        [self.loading stopAnimating];
    }];
}



-(void)restConnection:(RestConnection *)restConnection failedEntity:(NSString *)entity error:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
-(void)restConnection:(RestConnection *)restConnection succeededEntity:(NSString *)entity
{
    [self displayInfo];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}



@end
