//
//  AmbianceDetailsViewController.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 14/09/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AmbiancePhotos.h"
#import "AmbianceProductsDAO.h"
#import "AmbiancesDAO.h"
#import "AmbianceView.h"
#import "ProductViewController.h"
#import "AmbianceQuizViewController.h"
#import "NewScrapViewController.h"

#import "AmbianceScrollView.h"
#import "PropertiesHelper.h"

#import "NewScrapViewController.h"

@interface AmbianceDetailsViewController : UIViewController <UIScrollViewDelegate>
{
    CGSize screenSize;
    Ambiances * ambiance;
}

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) NSMutableArray *pageViews;

@property (nonatomic, strong) IBOutlet UIBarButtonItem * btnQuiz;
@property (nonatomic, strong) NSArray * ambiancePhotos;

@end
