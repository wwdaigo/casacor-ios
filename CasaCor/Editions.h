//
//  Editions.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 27/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Editions : NSManagedObject

@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * oid;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * ticketUrl;
@property (nonatomic, retain) NSString * votingUrl;
@property (nonatomic, retain) NSString * map;

@end
