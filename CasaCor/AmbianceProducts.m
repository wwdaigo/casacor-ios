//
//  AmbianceProducts.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 24/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "AmbianceProducts.h"


@implementation AmbianceProducts

@dynamic ambiancePhotoId;
@dynamic oid;
@dynamic posX;
@dynamic posY;
@dynamic productId;

@end
