//
//  MainTitleView.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 16/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTitleView : UIView


@property (nonatomic, strong) IBOutlet UILabel * lblCity;

-(void)setCity:(NSString *)city;

@end
