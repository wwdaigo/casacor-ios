//
//  AbstractTableDataSource.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 15/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AbstractTableDataSource : NSObject

@property (nonatomic, strong) NSMutableArray * list;

@end
