//
//  PropertiesHelper.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 17/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PropertiesHelper : NSObject

+(void)setChosenEdition:(NSNumber *)oid;
+(NSNumber *)chosenEdition;

+(void)setQuizOpened:(NSNumber *)oid;
+(BOOL)quizOpened:(NSNumber *)oid;


+(void)setName:(NSString *)name;
+(NSString *)name;

+(void)setEmail:(NSString *)email;
+(NSString *)email;

@end
