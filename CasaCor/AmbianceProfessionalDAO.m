//
//  AmbienceProfessionalDAO.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 14/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "AmbianceProfessionalDAO.h"

@implementation AmbianceProfessionalDAO

-(id)init
{
    [super setEntityName:@"AmbianceProfessional"];
    
    if (self = [super init])
    {
        
    }
    
    return self;
}

-(AmbianceProfessional *)objectByAmbienceId:(NSNumber *)ambienceId professionalId:(NSNumber *)professionalId
{
    NSString * query = [NSString stringWithFormat:@"ambianceId == %@ and professionalId == %@",ambienceId, professionalId];
    NSArray * searchArray = [super search:query];
    
    if ([searchArray count] == 0)
    {
        AmbianceProfessional * object = [NSEntityDescription insertNewObjectForEntityForName:super.entityName inManagedObjectContext:moc];
        [object setProfessionalId:professionalId];
        [object setAmbianceId:ambienceId];
        
        return object;
    }
    else
    {
        return [searchArray firstObject];
    }
}

@end
