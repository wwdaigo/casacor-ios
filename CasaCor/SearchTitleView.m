//
//  SearchTitleView.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 03/09/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "SearchTitleView.h"

@implementation SearchTitleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    id barButtonAppearanceInSearchBar = [UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil];
    [barButtonAppearanceInSearchBar setTitle:@"Cancelar"];
}

@end
