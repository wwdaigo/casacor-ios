//
//  ProductButtonView.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 27/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "ProductButtonView.h"

@implementation ProductButtonView


-(void)layoutSubviews
{
    [super layoutSubviews];
    [self createView];
}

-(void)createView
{
    if (!created)
    {
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 11, 14, 22)];
        [self.imageView setImage:[UIImage  imageNamed:@"product-tag"]];
        [self addSubview:self.imageView];
        
        self.button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        [self.button setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.button];
        
        [self.button addTarget:self action:@selector(tap:) forControlEvents:UIControlEventTouchUpInside];
        
        [self rotataleRight:self.imageView];
    }
    
    created = YES;
}

-(void)tap:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"productButtonTapped" object:self.productID];
}

-(void)adjustPositionWithSize:(CGRect)rect
{
    CGFloat posX = [self.posX floatValue];
    CGFloat posY = [self.posY floatValue];


    CGRect frame = CGRectMake(rect.origin.x+(rect.size.width * posX)-22, rect.origin.y + (rect.size.height * posY)-22, 44, 44);
    [self setFrame:frame];
}

-(void)rotataleRight:(UIView *)view
{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.transform = CGAffineTransformMakeRotation(-.4);
                     }
                     completion:^(BOOL finished) {
                         
                         [self performSelector:@selector(rotataleLeft:) withObject:view afterDelay:0.05];
                         
                     }];
    
}

-(void)rotataleLeft:(UIView *)view
{
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.transform = CGAffineTransformMakeRotation(.4);
                     }
                     completion:^(BOOL finished) {
                         
                         [self performSelector:@selector(rotataleRight:) withObject:view afterDelay:0.05];
                         
                         
                     }];
    
}

@end
