//
//  AbstractTableDataSource.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 15/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "AbstractTableDataSource.h"

@implementation AbstractTableDataSource

-(id)init
{
    if (self = [super init])
    {
        self.list = [NSMutableArray new];
    }
    return self;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.list.count;
}

@end
