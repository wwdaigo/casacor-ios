//
//  ProfessionalsDAO.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 13/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "DAOAbstract.h"

#import "Professionals.h"

@interface ProfessionalsDAO : DAOAbstract

-(Professionals *)objectByOID:(NSNumber *)oid;

@end
