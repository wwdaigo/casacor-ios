//
//  ProductsDAO.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 13/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "DAOAbstract.h"

#import "Products.h"

@interface ProductsDAO : DAOAbstract

-(Products *)objectByOID:(NSNumber *)oid;

@end
