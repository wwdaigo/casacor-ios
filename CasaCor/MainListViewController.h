//
//  MainListViewController.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 14/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "ListViewController.h"

#import "AppDelegate.h"

#import "MainTitleView.h"
#import "EditionsDAO.h"

#import "PropertiesHelper.h"

#import "ProfessionalViewController.h"

#import "RestConnection.h"
#import "ImageHelper.h"

#import "AmbiancePhotosDAO.h"
#import "AmbiancesDAO.h"
#import "PropertiesHelper.h"
#import "AmbianceProfessionalDAO.h"
#import "ProfessionalsDAO.h"
#import "SearchTitleView.h"

#import "AmbianceDetailsViewController.h"

#import "CloudinaryUtils.h"

@interface MainListViewController : ListViewController <RestConnectionDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate, UITableViewDelegate>
{
    MainTitleView * mainTitleView;
    SearchTitleView * searchTitleView;
    
    UIBarButtonItem * searchButton;
    
    NSMutableArray * list;
    NSMutableArray * filteredList;
    
    BOOL isSearching;
    
    BOOL sponsorDisplayed;
    
    IBOutlet NSLayoutConstraint * bottomConstraint;
}


@end
