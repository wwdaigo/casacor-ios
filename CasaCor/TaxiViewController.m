//
//  TaxiViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 27/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "TaxiViewController.h"
#import "PropertiesHelper.h"
#import "EditionsDAO.h"

@interface TaxiViewController ()

@end

@implementation TaxiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];

    if(sysVer >= 8)
    {
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]))
        {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"])
            {
                [self.locationManager requestAlwaysAuthorization];
            }
            else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"])
            {
                [self.locationManager  requestWhenInUseAuthorization];
            }
            else
            {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
        
        [self.locationManager startUpdatingLocation];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if (newLocation != nil)
    {
        [self.locationManager stopUpdatingLocation];
        
        if (!requested)
        {
            NSLog(@"didUpdateToLocation: %@", newLocation);
            
            requested = YES;
            
            EditionsDAO * dao = [EditionsDAO new];
            Editions * edition = [dao objectByOID:[PropertiesHelper chosenEdition]];
            
            [self openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://webapp-ryu.easytaxi.net.br/external/partner/?pick=%f,%f,17z&refp=home&dest=%@,%@,17z&refd=job&part=TripAdvisor&utm_campaign=trip_advisor_partner&utm_source=trip_advisor&utm_medium=link&lang=en_us", newLocation.coordinate.latitude, newLocation.coordinate.longitude, edition.latitude, edition.longitude]]];
            
        }
    }
}

@end
