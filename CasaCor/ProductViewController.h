//
//  ProductViewController.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 25/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RestConnection.h"
#import "ProductsDAO.h"
#import "SupplierDAO.h"

#import "MBProgressHUD.h"

#import "ProductsPhotosViewController.h"
#import "ImageHelper.h"

@interface ProductViewController : UIViewController <RestConnectionDelegate>
{
    Products * product;
}
@property (nonatomic, strong) IBOutlet UIImageView * imageView;
@property (nonatomic, strong) IBOutlet UILabel * lblTitle;
@property (nonatomic, strong) IBOutlet UILabel * lblSupplier;
@property (nonatomic, strong) IBOutlet UILabel * lblSegment;
@property (nonatomic, strong) IBOutlet UILabel * lblPrice;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView * loading;

@property (nonatomic, strong) NSNumber * productId;

@end
