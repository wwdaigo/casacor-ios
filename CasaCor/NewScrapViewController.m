//
//  NewScrapViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 25/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "NewScrapViewController.h"

@interface NewScrapViewController ()

@end

@implementation NewScrapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [txtName setText:[PropertiesHelper name]];
    [txtEmail setText:[PropertiesHelper email]];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardNotification:) name:UIKeyboardDidChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleKeyboardHide:) name:UIKeyboardDidHideNotification object:nil];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
}

-(BOOL)validate
{
    if ([txtName.text isEqualToString:@""])
    {
        [self displayError:@"Digite o seu nome."];
        [txtName becomeFirstResponder];
        return NO;
    }
    else if ([txtEmail.text isEqualToString:@""])
    {
        [self displayError:@"Digite o seu e-mail."];
        [txtEmail becomeFirstResponder];
        return NO;
    }
    else if (![self validateEmail:txtEmail.text])
    {
        [self displayError:@"E-mail inválido."];
        [txtEmail becomeFirstResponder];
        return NO;
    }
    else if ([txtMessage.text isEqualToString:@""])
    {
        [self displayError:@"Digite seu comentário."];
        [txtMessage becomeFirstResponder];
        return NO;
    }
    return YES;
}

-(void)hideKeyboard
{
    [txtName resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtMessage resignFirstResponder];
}

-(void)displayError:(NSString *)message
{
    [[[UIAlertView alloc] initWithTitle:@"Erro!" message:message delegate:nil cancelButtonTitle:@"Fechar" otherButtonTitles:nil] show];
}

- (BOOL) validateEmail: (NSString *) candidate
{
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

-(IBAction)sendMessage:(id)sender
{

    if ([self validate])
    {
        [self hideKeyboard];
        
        [PropertiesHelper setName:txtName.text];
        [PropertiesHelper setEmail:txtEmail.text];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        RestConnection * connpost = [RestConnection new];
        [connpost setDelegate:self];
        [connpost postMessageWithName:txtName.text email:txtEmail.text ambianceId:self.ambiance.oid message:txtMessage.text];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)restConnection:(RestConnection *)restConnection failedEntity:(NSString *)entity error:(NSError *)error
{
    
}

-(void)restConnection:(RestConnection *)restConnection succeededEntity:(NSString *)entity
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [[[UIAlertView alloc] initWithTitle:@"Sucesso!" message:@"Mensagem enviada com sucesso!" delegate:self cancelButtonTitle:@"Fechar" otherButtonTitles:nil] show];
}

- (void)handleKeyboardNotification:(NSNotification *)aNotification
{
    NSDictionary* keyboardInfo = [aNotification userInfo];
    NSValue* keyboardFrame = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameRect = [keyboardFrame CGRectValue];
    
    bottomConstraint.constant = keyboardFrameRect.size.height;
}

- (void)handleKeyboardHide:(NSNotification *)aNotification
{
    bottomConstraint.constant = 0;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [[self navigationController] popViewControllerAnimated:YES];
}


@end
