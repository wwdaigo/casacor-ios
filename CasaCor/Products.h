//
//  Products.h
//  CasaCor
//
//  Created by Daigo Matsuoka on 25/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Products : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * oid;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * segment;
@property (nonatomic, retain) NSNumber * supplierId;
@property (nonatomic, retain) NSString * photo;

@end
