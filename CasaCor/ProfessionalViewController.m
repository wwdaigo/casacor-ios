//
//  ProfessionalViewController.m
//  CasaCor
//
//  Created by Daigo Matsuoka on 17/08/15.
//  Copyright (c) 2015 Muuving. All rights reserved.
//

#import "ProfessionalViewController.h"
#import "NewScrapViewController.h"

#import "ImageHelper.h"
#import "CloudinaryUtils.h"

@interface ProfessionalViewController ()

@end

@implementation ProfessionalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
 
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    [self.lblName setText:[self.dataObject.name uppercaseString]];
    [self.lblEmail setText:self.dataObject.email];
    [self.lblWebsite setText:self.dataObject.website];
    
    NSString * filenameProf = [NSString stringWithFormat:@"pro-%@.jpg",self.dataObject.oid];
    NSString * filePathProf = [documentsDirectory stringByAppendingPathComponent:filenameProf];

    //[self.imgPhoto setImage:[UIImage imageNamed:filePathProf]];
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    [ImageHelper imageWithURLString:[CloudinaryUtils formatImageUrl:[self.dataObject photo] width:screenSize.width*2 height:480] prefix:@"probig" oid:[self.dataObject oid] andCompletionHandler:^(UIImage *image) {
        [self.imgPhoto setImage:image];
        [self.loading stopAnimating];
    }];
    
    //
    
    [self showContactInfo];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    RestConnection * rest = [RestConnection new];
    [rest setDelegate:self];
    [rest getProfessionalWithId:self.dataObject.oid];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)showContactInfo
{
    if (![[self.dataObject email] isEqualToString:@""] && [self.dataObject email] != nil)
    {
        [self.imgEmail setHidden:NO];
        [self.lblEmail setText:self.dataObject.email];
    }
    
    if (![[self.dataObject website] isEqualToString:@""] && [self.dataObject website] != nil)
    {
        [self.imgWeb setHidden:NO];
        [self.lblWebsite setText:self.dataObject.website];
    }
}

#pragma mark - Navigation

-(IBAction)goEmail:(id)sender
{
    if (![self.dataObject.email isEqualToString:@""])
    {
        NSString *email = [NSString stringWithFormat:@"mailto:%@&subject=",self.dataObject.email];
        email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    }
}

-(IBAction)goWeb:(id)sender
{
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",self.dataObject.website]];

    if (url!=nil)
    {
        [[UIApplication sharedApplication] openURL:url];
    }
        
}


-(void)restConnection:(RestConnection *)restConnection failedEntity:(NSString *)entity error:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self showContactInfo];
}

-(void)restConnection:(RestConnection *)restConnection succeededEntity:(NSString *)entity
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self showContactInfo];
}

@end
